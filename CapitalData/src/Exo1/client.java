package Exo1;

public class client {
	
	private int id;
	private String prenom;
	private String nom;
	private String mail;
	private String dateInscription;
	
	@Override
	public String toString() {
		return "" + id + " ==>[ prenom=" + prenom + ", nom=" + nom + ", mail=" + mail + ", dateInscription="
				+ dateInscription + "]\n";
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(String dateInscription) {
		this.dateInscription = dateInscription;
	}
	public client() {
		super();
	}
	
	

}
