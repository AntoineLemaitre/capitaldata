package Exo1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import java.util.List;

public class CSVparseur {
	
	public List<client> parseCSV(String filename) {
		
		List<client> ListeClientParser = new ArrayList<client>();
		int id=0;
		
		try (BufferedReader br = Files.newBufferedReader(Paths.get(filename))) {

		    // CSV file delimiter
		    String DELIMITER = ",";

		    // read the file line by line
		    String line;
		    
		    while ((line = br.readLine()) != null) {

		        // convert line into columns
		        String[] columns = line.split(DELIMITER);
		        client clientToAdd = new client();
		        
		        clientToAdd.setPrenom(columns[0]);
		        clientToAdd.setNom(columns[1]);
		        clientToAdd.setMail(columns[2]);
		        clientToAdd.setDateInscription(columns[3]);
				clientToAdd.setId(id);
		        id++;
		        
		        ListeClientParser.add(clientToAdd);
		    }

		} catch (IOException ex) {
		    ex.printStackTrace();
		}
		return ListeClientParser;
	}
	
	public List<client> appendCSV(String filename, List<client> data) {
		

		int id=data.size();
		
		try (BufferedReader br = Files.newBufferedReader(Paths.get(filename))) {

		    // CSV file delimiter
		    String DELIMITER = ",";

		    // read the file line by line
		    String line;
		    
		    while ((line = br.readLine()) != null) {

		        // convert line into columns
		        String[] columns = line.split(DELIMITER);
		        client clientToAdd = new client();
		        clientToAdd.setPrenom(columns[0]);
		        clientToAdd.setNom(columns[1]);
		        clientToAdd.setMail(columns[2]);
		        clientToAdd.setDateInscription(columns[3]);
		        
				clientToAdd.setId(id);
		        id++;
		        data.add(clientToAdd);

		    }

		} catch (IOException ex) {
		    ex.printStackTrace();
		}
		
		return data;
	}

	public void CreateJsonFile(String filename) throws IOException {
		
		 try {
			  
	            // Recevoir le fichier 
	            File f = new File(filename+".json");
	  
	            // Cr�er un nouveau fichier
	            // V�rifier s'il n'existe pas
	            if (f.createNewFile())
	                System.out.println("File created");
	            else
	                System.out.println("File already exists");
	        }
	        catch (Exception e) {
	            System.err.println(e);
	        }
		 
		 Path path = Paths.get(filename+".json");
		 
		 String strfinal="";
		 String str = "{\"contact\":[";
			byte[] bs = str.getBytes();
			Files.write(path, bs);
		 strfinal=strfinal.concat(str);

		 int id=0;
			
			try (BufferedReader br = Files.newBufferedReader(Paths.get(filename))) {

			    // CSV file delimiter
			    String DELIMITER = ",";

			    // read the file line by line
			    String line;
			    
			    while ((line = br.readLine()) != null) {

			        // convert line into columns
			        String[] columns = line.split(DELIMITER);
			        client clientToAdd = new client();
			        clientToAdd.setPrenom(columns[0]);
			        clientToAdd.setNom(columns[1]);
			        clientToAdd.setMail(columns[2]);
			        clientToAdd.setDateInscription(columns[3]);
			        
					clientToAdd.setId(id);
			        id++;
			        
			        
				 	String str1 = "{"
				 			+ "\"firstname\""+":"+"\""+clientToAdd.getPrenom()+"\","
				 			+ "\"lastname\""+":"+"\""+clientToAdd.getNom()+"\","
				 			+ "\"email\""+":"+"\""+clientToAdd.getMail()+"\","
				 			+ "\"inscription_date\""+":"+"\""+clientToAdd.getDateInscription().replace('T', ' ').replaceAll("Z", "")+"\""
				 			+ "},";
					byte[] bs1 = str1.getBytes();
					Files.write(path, bs1);
					strfinal=strfinal.concat(str1);

			    }

			} catch (IOException ex) {
			    ex.printStackTrace();
			}
		 
			 String str1 = "{}]}";
				byte[] bs1 = str1.getBytes();
				Files.write(path, bs1);
				strfinal=strfinal.concat(str1);
				byte[] bs11 = strfinal.getBytes();
				Files.write(path, bs11);
				System.out.println("Json creer et rempli");
		 
		 
		 
		 
		 	
		
	}
	
	public void CreateJsonFileAvecErreur( String filename) throws IOException {
		try {
			  
            // Recevoir le fichier 
            File f = new File(filename+".json");
  
            // Cr�er un nouveau fichier
            // V�rifier s'il n'existe pas
            if (f.createNewFile())
                System.out.println("File created");
            else
                System.out.println("File already exists");
        }
        catch (Exception e) {
            System.err.println(e);
        }
	 
	 Path path = Paths.get(filename+".json");
	 String strfinal="";
	 String str = "{\"contact\":[";
		byte[] bs = str.getBytes();
		Files.write(path, bs);
	 strfinal=strfinal.concat(str);
	 
	 int id=0;
	 List<client>clientValide = new ArrayList<client>();
	 List<String>emailValide=new ArrayList<String>();
	 int lignetotal=0;
		
		try (BufferedReader br = Files.newBufferedReader(Paths.get(filename))) {

		    // CSV file delimiter
		    String DELIMITER = ",";

		    // read the file line by line
		    String line;
		    
		    while ((line = br.readLine()) != null) {

		    	lignetotal++;
		        // convert line into columns
		        String[] columns = line.split(DELIMITER);
		        if (columns.length!=4) {
		        	System.err.println("PB colums taille: "+columns.length);
					
				}
		        else {
		        	client clientToAdd = new client();
			        
					id++;
			        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
			        String email = columns[2];
			        Boolean a = email.matches(EMAIL_REGEX);
			        if (a==true) {
						//System.err.println("OK email: "+email);
					}else {
						System.err.println("PB email: "+email);
					}
			        Boolean b=false;
			        if (columns[0].length()<50) {
						b=true;
					}else {
						System.err.println("PB prenom: "+columns[0]);
					}
			        Boolean c=false;
			        if (columns[1].length()<50) {
						c=true;
					}else {
						System.err.println("PB nom: "+columns[1]);
					}
			        Boolean d=false;
			        if (columns[3].length()<255 && (columns[3].contains("T") || columns[3].contains("/"))) {
						d=true;
					}else {
						System.err.println("PB Date: "+columns[3]);
					}
			        
			        
			        
			        	        
					if (a==b==c==d==true) {
						
						clientToAdd.setPrenom(columns[0]);
				        clientToAdd.setNom(columns[1]);
				        clientToAdd.setMail(columns[2]);
				        emailValide.add(columns[2]);
				        clientToAdd.setDateInscription(columns[3]);
						clientToAdd.setId(id);
						
						clientValide.add(clientToAdd);
							
					}
				}
		        
			 	
			 	
		    }

		} catch (IOException ex) {
		    ex.printStackTrace();
		}
		long compteClientValide = emailValide.stream().distinct().count();
	clientValide.stream().distinct().forEach(client->{
	
		
		if (emailValide.contains(client.getMail())) {
			
			String str1 = "{"
		 			+ "\"firstname\""+":"+"\""+client.getPrenom()+"\","
		 			+ "\"lastname\""+":"+"\""+client.getNom()+"\","
		 			+ "\"email\""+":"+"\""+client.getMail()+"\","
		 			+ "\"inscription_date\""+":"+"\""+client.getDateInscription().replace('T', ' ').replaceAll("Z", "")+"\""
		 			+ "},";
			byte[] bytes = str1.getBytes();
			try {
				Files.write(path, bytes, StandardOpenOption.APPEND);
			List<String> listeEmailToRemove = new ArrayList<String>();
			
			listeEmailToRemove.add(client.getMail());
			emailValide.removeAll(listeEmailToRemove);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			
		}
		
	});
		
		 String str1 = "{}]}";
			byte[] bytes = str1.getBytes();
			Files.write(path, bytes, StandardOpenOption.APPEND);
			long erreurs=lignetotal-clientValide.size();
			System.out.println("Json creer et rempli");
			System.out.println("Il y a "+lignetotal+" Lignes dans le CSV");
			System.out.println("Il a "+clientValide.size()+" clients valide Avec Doublon Possible");
			System.out.println("Il a "+compteClientValide+" clients unique valide dans le json");
			System.out.println("Il a "+erreurs+" clients non valide");
			erreurs=clientValide.size()-compteClientValide;
			System.out.println("Il a "+erreurs+" clients  doublon");
			
	 
	}
}
